package com.goostree.codewars.yourorderplease;

import java.util.Scanner;

public class Order {
	  public static String order(String words) {
		  
		  if(null != words && !words.equals("")) {
		  
			  String[] input = words.split(" ");
			  String result[] = new String[input.length];
			  String resultStr = "";
			  
			  for(String word : input){
				  result[new Scanner(word).useDelimiter("\\D+").nextInt() - 1] = word;
			  }
			  
			  for(int i = 0; i < result.length; i++) {
				  resultStr += (i == 0) ? result[i] : " " + result[i];
			  }
			  words = resultStr;
		  }
		  
		  // Java 8:
		  //Arrays.stream(words.split(" "))
	      //.sorted(Comparator.comparing(s -> Integer.valueOf(s.replaceAll("\\D", ""))))
	      //.reduce((a, b) -> a + " " + b).get();
		  
		  return words;
	  }
	  
	  public static void main(String[] args){
		  System.out.println(order("is2 Thi1s T4est 3a"));
	  }
}
